<?php

namespace App\Http\Controllers;
use App\Models\Post;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class NotizenController extends Controller
{   
    use HasFactory;

    #Create a new note in DB
    public function create()
    {
        $response = Post::create(["name" => "", "text" =>"", "datae" => "", "done" => ""]);
        return $response;
    }

    #Show a new note in DB
    public function show(string $id): string
    {
        $user = Post::firstWhere('id', $id)->getAttribute("text");
        return $user;
    }
    
    #Change an existing note in the DB 
    public function change(string $id,string $newName, string $newText,string $newDate,string $newDone)
    {
        $note = Post::find($id);
        $note -> text = $newText;
        $note -> date = $newDate;
        $note -> done = $newDone;
        $note -> name = $newName;
        $note -> save();
    }



    

}
