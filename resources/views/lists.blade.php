@extends('layouts.layout')

@section('content')
    <h1>Create Notes</h1>

    <button id="add-note-button">Add Note</button>

    <div id="boxes-container"></div>

    <style>
        .box {
            border: 1px solid #ddd;
            padding: 10px;
            margin-bottom: 10px;
            width: 400px;
        }

        .box span {
            font-weight: bold;
        }

        .box button {
            float: right;
            margin-left: 10px;
        }
    </style>

    <script>
        const addNoteButton = document.getElementById('add-note-button');
        const boxesContainer = document.getElementById('boxes-container');
        let boxCount = 1;

        addNoteButton.addEventListener('click', () => {
            const newBox = document.createElement('div');
            newBox.classList.add('box');

            const boxNumber = document.createElement('span');
            boxNumber.textContent = `Note ${boxCount}`;
            newBox.appendChild(boxNumber);

             const deleteButton = document.createElement('button');
            deleteButton.textContent = 'Delete';
            deleteButton.addEventListener('click', () => {
                boxesContainer.removeChild(newBox);
            });
            newBox.appendChild(deleteButton);

            const editButton = document.createElement('button');
            editButton.textContent = 'Edit';
            editButton.addEventListener('click', () => {
                
            });
            newBox.appendChild(editButton);

            boxesContainer.appendChild(newBox);
            boxCount++;
        });
    </script>
@endsection
