<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Notebook</title>
    <style>
        body {
            background-color: #dddddd;
            font-family: Arial, sans-serif; 
            margin: 0;
            padding: 0;
            height: 100vh;
            
        }

        .header {
            background-color: #5ac772; 
            color: #fff;
            padding: 10px;
            text-align: center;
        }

        .container {
        display: flex;
        flex-direction: column; 
        align-items: center; 
        padding: 20px;
        overflow-y: auto; 
       }
        .box {
            width: 60%;
            height: 150px;
            background-color: #fff; 
            margin: 10px;
            border-radius: 5px;
            box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1); 
        }
    </style>
</head>
<body>
    <div class="header">
        <h1>Notizbuch</h1>
    </div>

    <div class="container">
        @yield('content')
    </div>
</body>
</html>
