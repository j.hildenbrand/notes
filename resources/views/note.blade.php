

@extends('layouts.layout')


@section('content')
    <h1>Note  {{ $id }}</h1>

    <form class="mt-4">
        @csrf
        <div class="form-group">
            <textarea class="form-control @error('note') is-invalid @enderror" id="note" name="note" rows="15" required> </textarea>
        </div>

        <button type="submit" class="btn btn-primary" >
            <a href="{{ url('/list/') }}" class="btn btn-xs btn-info pull-right">Save Note</a>
        </button>
    </form>
@endsection




<style>
.container{
    height:90%;
    width: 70%;
    margin-left: 12.5%;
}
.mt-4 {
    width: 100%;
    height: 100%;
    margin: 0 auto;

} 
.form-control {
    width: 100%;
    margin: 0 auto;
    height: 100%;
}

.form-group {
    width: 100%;
    margin: 0 auto;
    height: 80%;

} 

h1 {
    text-align: center;
}

.form-group {
    margin-bottom: 2rem;
}

textarea {
    font-size: 16px;
    min-height: 300px; 
    resize: vertical;
}
</style>