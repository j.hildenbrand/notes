<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\NotizenController;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/list', function () {

    return view('lists');
});


Route::get("/note/{id}", function($id){
    return view("note", compact("id"));
});

#Controller Routes
Route::resource('/note/{id}', [UserController::class,'show']);

Route::resource('/note/{id}', [UserController::class, 'change']);

Route::resource('/list/{id}', [UserController::class, 'create']);




