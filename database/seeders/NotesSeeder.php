<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;


class NotesSeeder extends Seeder
{
    public function run(): void
    {
        DB::table('notizen')->insert([
            'name' => Str::random(10),
            'text' => Str::random(10),
            'date' => Str::random(10),
            'done' => Str::random(10),
        ]);
    } //
    
}
